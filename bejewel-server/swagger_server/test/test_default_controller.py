# coding: utf-8

from __future__ import absolute_import

from swagger_server.models.error_model import ErrorModel
from swagger_server.models.inline_response200 import InlineResponse200
from swagger_server.models.player import Player
from . import BaseTestCase
from six import BytesIO
from flask import json


class TestDefaultController(BaseTestCase):
    """ DefaultController integration test stubs """

    def test_create_player(self):
        """
        Test case for create_player

        
        """
        player = Player()
        response = self.client.open('/api/player',
                                    method='POST',
                                    data=json.dumps(player),
                                    content_type='application/json')
        self.assert200(response, "Response body is : " + response.data.decode('utf-8'))

    def test_player_ranking(self):
        """
        Test case for player_ranking

        
        """
        response = self.client.open('/api/player/ranking',
                                    method='GET',
                                    content_type='application/json')
        self.assert200(response, "Response body is : " + response.data.decode('utf-8'))

    def test_update_player(self):
        """
        Test case for update_player

        
        """
        player = Player()
        response = self.client.open('/api/player/{id}'.format(id=56),
                                    method='PUT',
                                    data=json.dumps(player),
                                    content_type='application/json')
        self.assert200(response, "Response body is : " + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
