import connexion
from swagger_server.models.error_model import ErrorModel
from swagger_server.models.inline_response200 import InlineResponse200
from swagger_server.models.player import Player
from datetime import date, datetime
from typing import List, Dict
from six import iteritems
from ..util import deserialize_date, deserialize_datetime
from ..database.dao.player import PlayerDAO

def create_player(player):
    """
    create_player
    Creates a new player
    :param player: Player with score
    :type player: dict | bytes

    :rtype: InlineResponse200
    """
    if connexion.request.is_json:
        player = Player.from_dict(connexion.request.get_json())

    player_dao = PlayerDAO()
    db_player_id = player_dao.create_player(player.name, player.score)
    print(db_player_id)
    if db_player_id:
        return db_player_id, 200
    else:
        return ErrorModel(code=400,message="Player cannot be created"), 400


def player_ranking():
    """
    player_ranking
    Gets tops 6 players

    :rtype: List[Player]
    """
    player_dao = PlayerDAO()
    player_db_list = player_dao.get_top_players()
    player_list = []
    for player_db in player_db_list:
        player_list.append(Player(name=player_db.username, score=player_db.score))
    print(player_list)
    if len(player_list) == 0:
        return "No players registered", 204
    else:
        return player_list, 200

def update_player(id, player):
    """
    update_player
    Updates a new player
    :param id: The player ID
    :type id: int
    :param player: Player with score
    :type player: dict | bytes

    :rtype: InlineResponse200
    """
    if connexion.request.is_json:
        player = Player.from_dict(connexion.request.get_json())

    player_dao = PlayerDAO()
    db_player_id = player_dao.update_player(id, player.score)
    print(db_player_id)
    if db_player_id:
        return db_player_id, 200
    else:
        return ErrorModel(code=400,message="Player cannot be created"), 400

