import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Text, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from datetime import datetime

FILE_PATH = os.path.dirname(os.path.abspath(__file__))

engine = create_engine('sqlite:///' + FILE_PATH + '/bejewel.db', echo=True, poolclass=NullPool, connect_args={'check_same_thread': False})
Base = declarative_base(bind=engine)



class Player(Base):
    __tablename__ = "player"

    id = Column(Integer, primary_key=True)
    username = Column(String(50), nullable=False)
    score = Column(Integer)

    def __init__(self, username, score):
        self.username = username
        self.score = score


def setup_db(flush=False, initialize=False):
    if flush:
        Base.metadata.drop_all()

    Base.metadata.create_all()

    if initialize:
        initial_data()

def initial_data():

    current_session = scoped_session(sessionmaker(bind=engine));

    current_session.add_all([
        Player('Player5', 2600),
        Player('Player9', 2000),
        Player('Player2', 100),
        Player('Player3', 200)])

    current_session.commit()
    current_session.remove()

# Gets db session to make updates into db

class DBConnection(object):
    __engine = engine


    def __init__(self):
        pass

    @classmethod
    def get_session(cls):
        return scoped_session(sessionmaker(bind=engine))


