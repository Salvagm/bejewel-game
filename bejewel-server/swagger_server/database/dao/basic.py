import logging
import sys
import os
from datetime import datetime

from ..db_utils import DBConnection

logger = logging.getLogger(__name__)

class BasicDAO(object):

    def __init__(self):
        self.__current_session = None


    @property
    def session(self):
        if not self.__current_session:
            self.__current_session = DBConnection.get_session()

        return self.__current_session

    def close_session(self):
        if self.__current_session:
            self.__current_session.remove()

        self.__current_session = None
