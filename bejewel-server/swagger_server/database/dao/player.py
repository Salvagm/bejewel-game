import logging
import sys
import os
from datetime import datetime

from sqlalchemy import exc
from ..db_utils import DBConnection, Player
from .basic import BasicDAO

logger = logging.getLogger(__name__)

class PlayerDAO(BasicDAO):

    def __init__(self):
        super(PlayerDAO, self).__init__()

    def get_top_players(self, limit=6):

        user_list = self.session.query(Player).order_by(Player.score.desc()).limit(limit)
        self.close_session()

        return user_list

    def create_player(self, name, score):
        new_player = Player(name, score)
        result = None
        try:
            self.session.add(new_player)
            self.session.commit()
            result = new_player.id
        except :
            logger.exception("User cannot be created")
        finally:
            self.close_session()

        return result

    def update_player(self, player_id, score):
        result = None
        try:
            player_model = self.session.query(Player).get(player_id)
            print(player_model)
            player_model.score = score

            self.session.add(player_model)
            self.session.commit()

            result = player_model.id
        except:
            logger.exception("Error trying update raw device")
            self.session.rollback()

        finally:
            self.close_session()

        return result
