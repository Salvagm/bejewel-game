#!/usr/bin/env python3

import connexion
from .encoder import JSONEncoder
from .database import db_utils


if __name__ == '__main__':
    db_utils.setup_db(True, True)

    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Bejewel game server to show and store player info'})
    app.run(host="localhost", port=8182)
