# Bejewel-GAME
Bejewel like game made in python. In this game the player will get score every time 3 or more shape are matched. The project is divided in two parts: 
- __Client__: Manage the game logic, graphics, etc
- __Server__: Saves and server user related data

Once the player gets score it will be uploaded to the server and he will get realtime ranking result from the best 6 players.

### Table of content
- [Requirements](#requirements)
- [Installation](#installation)
- [Run project](#run-project)
- [Game information](#game-information)
- [Server information](#server-information)


## Requirements
Bejewel-game requires python3.3+ to run.
- __Window__: downlod and install python [here](https://www.python.org/ftp/python/3.6.2/python-3.6.2.exe)
  -   Set python path in your system while python is installed
-  __Linux__: Install throw your distro package
   - debian: 
        - `sudo apt-get install python3`
        - `sudo apt-get install python3-pip`

## Installation
Befour run the game or the server is necessary to install all the libraries
- __Windows__: Execute the script in this project `install-dependencies.bat`
- __Linux__: You can install all the libraries globally. In this case we will install them locally to this project
    - In bejewel-game folder:
        - run `pip3 install -t 3rd -r requirements.txt`
    - In bejewel-server folder:
        - run `pip3 install -t 3rd -r requirements.txt`

## Run project
Once all the libraries are installed
- __Windows__: This project contains to different execution scripts:
    - `run_game.bat` will start the game
    - `run_server.bat` will start the server
- __Linux__: To run the game and ther server in linux from project root:
    - `PYTHONPATH=bejewel-game/3rd python3 -m bejewel-game`
    - `PYTHONPATH=bejewel-server/3rd python3 -m bejewel-server/swagger_server`

## Game information
### Folder structure
- __bejewel-game__ package contains the main module `Game.py`
- __bejewel-game/lib__ package contains external libraries
- __bejewel-game/resources__ folder contains all images
- __bejewel-game/scenes__ package contains all scenes in the game
- __bejewel-game/scenes/board__ package contains bejewel board for `level` scene
- __bejewel-game/scenes/hud__ pacakge contains game user interface for `level` scene
- __bejewel-game/services__ package constains game server conection

### Explanation
Game entity will initialize the window using pygame, it will store the player data, manage the scene and control the game cicle. In this project the game cicle will be __input events__->__logic update__->__render__. Game class is using Singleton pattern to have only one instance while playing and it is exposing different game functionalities.

MainMenu scene gets player name before start the game. Once the player press enter to submit MainMenu will try to send player info to the server and it will call `change_scene` from Game entity to go to Level scene.

Level scene is composed by Board and UIPanel entities. Level handles mouse event and selections and send the data to Board and UIPanel. 

Board manages the logic to moving the shapes, making match and it draws the current board state. To interact with game board Board has different states:
- READY - Board is ready for new player interaction
- REVERT_MATCHING - Board is trying to make a new match
- FILLING - Board is adding new shapes after the match
- STATIC_MATCHING - Board will try to make new matches after filling empty cells (this state is not implemented in the game)

To avoid check all the game shapes, when a match is succesful, Board keeps only the empty cells after match. Empty cells are stored in a heap ordered by Y position in ascending order. Board will get new shapes from the top of the screen.

UIPanel manages different UI text and buttons logic and it draws the UI elements. After successful match UIPanel sends player's score and makes a request to get the current ranking. Once it gets external data it updates the screen with the new values. When player clicks the UIButtons check if the mouse is clicking in its area to handle button events.

Ranking service is using an [asynchronous request library](https://pypi.python.org/pypi/requests-futures/0.9.0) to make server request. This service is making asynchronous request to avoid stopping the game for server response. Once a request is created, we can choose to wait until server request or send a callback function who will proccess the data from the request

## Server information
### Folder structure
- __bejewel-server/swagger_server__ package constains main server execution 
- __bejewel-game/swagger_server/controller__ package contains API module
- __bejewel-game/swagger_server/database__ package contains database initialization
- __bejewel-game/swagger_server/database/dao__ package contains database access functions
- __bejewel-game/swagger_server/model__ package contains API models
- __bejewel-game/swagger_server/swagger__ folder contains API specification file generated by [swagger](https://swagger.io)
- __bejewel-game/swagger_server/test__ pacakge contains API test functions

### Explanation
The server is generated by swagger editor. It uses [conexion](https://pypi.python.org/pypi/connexion) to manage API calls. 
Internally to manage database transactions we are using [SQLAlchemy](https://www.sqlalchemy.org/). The database will be created by SQLAlchemy when the server is launched in case it is missing.

### API
| Endpoint | method | payload | returns
| ------ | ------ | ------ | -----
| player | POST | {name: <string> score: <int>} | id : <int>
| player/{id} | PUT | {name: <string> score: <int>} | id : <int>
| player/ranking | GET | none | [{name: <string> score: <int>}]
