
import json
from requests_futures.sessions import FuturesSession

class HTTP_METHODS:
    GET = 'GET'
    POST = 'POST'
    PUT = 'PUT'

class RankingService(object):
    """Gets data from server"""
    __url = 'http://localhost'
    __port = '8182'
    __headers = {'Content-Type': 'application/json'}
    __session = FuturesSession()

    @classmethod
    def call_api_request(cls, apiurl,method='GET', payload=None, headers=None, params=None, callback=None):

        try:
            if method == HTTP_METHODS.GET:
                rls = cls.__session.get(apiurl, background_callback=callback)
            elif method == HTTP_METHODS.POST:
                rls = cls.__session.post(apiurl, data=json.dumps(payload), headers=headers, background_callback=callback)
            elif method == HTTP_METHODS.PUT:
                rls = cls.__session.put(apiurl, data=json.dumps(payload), headers=headers, background_callback=callback)
            else:
                raise Exception("Invalid http method")

            return rls
        except Exception as exc:
            print("__call_api error: %s", exc)

    @classmethod
    def post_player(cls, player, callback=None):
        url = "%s:%s/api/player" % (cls.__url, cls.__port)
        payload = {"name": player.name, "score": player.score}
        return cls.call_api_request(url, HTTP_METHODS.POST, payload, cls.__headers, callback=callback)

    @classmethod
    def put_player(cls, player, callback=None):
        url = "%s:%s/api/player/%s" % (cls.__url, cls.__port, player.id)
        payload = {"score": player.score, "name": player.name}
        return cls.call_api_request(url, HTTP_METHODS.PUT, payload, cls.__headers, callback=callback)

    @classmethod
    def get_ranking(cls, callback=None):
        url = "%s:%s/api/player/ranking" % (cls.__url, cls.__port)
        return cls.call_api_request(url, HTTP_METHODS.GET, callback=callback)
