import pygame
from pygame.locals import *

from .scenes import Level, MainMenu
from .scenes.hud import Player
from . import CONFIG

class Game(object):

    __instance = None
    __can_instantiate = False
    def __init__(self):
        self._running = True
        self._display_surf = None
        self.width = CONFIG.BOARD_SIZE * CONFIG.TILE_WIDTH + CONFIG.USER_PANEL_WIDTH
        self.height = CONFIG.BOARD_SIZE * CONFIG.TILE_HEIGHT
        self.surface_size = self.width, self.height

        self.clock = clock = pygame.time.Clock()

        self.player = Player("No-name")

    def __new__(cls):
        if not Game.__can_instantiate:
            raise RuntimeError("Constructor cannot be called")
        else:
            Game.__can_instantiate = False
            Game.__instance = object.__new__(cls)
            return Game.__instance


    @staticmethod
    def instance():
        if not Game.__instance:
            Game.__can_instantiate = True
            return Game()

        return Game.__instance

    def on_init(self):
        pygame.init()
        self.player = Player("No-name")
        self.current_scene = MainMenu()
        self._display_surf = pygame.display.set_mode(self.surface_size, pygame.DOUBLEBUF, pygame.RESIZABLE)
        self._running = True

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if self.on_init() == False:
            self._running = False

        while( self._running ):
            self.clock.tick(10);
            events = pygame.event.get()

            for event in events:
                if event.type == pygame.QUIT:
                    self._running = False

            self.current_scene.handle_events(events)
            self.current_scene.update()
            self.current_scene.render(self._display_surf)

        self.on_cleanup()

    def change_scene(self, new_scene):
        if new_scene == "level":
            self.current_scene = Level()

    def screen(self):
        return self._display_surf

    def stop_game(self):
        self._running = False
