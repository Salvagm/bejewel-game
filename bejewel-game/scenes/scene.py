from abc import ABC, abstractmethod

class Scene(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def render(self, screen):
        return

    @abstractmethod
    def update(self):
        return

    @abstractmethod
    def handle_events(self, events):
        return
