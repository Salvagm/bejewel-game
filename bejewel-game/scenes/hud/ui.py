import pygame
from pygame.locals import *

from .logic import Ranking, Player
from ... import game

class UI_CONFIG:
    TITLE_MARGIN = 10
    LABEL_MARGIN = 30
    SCORE_MARGIN = 50

class COLORS:
    # Define the colors we will use in RGB format
    BLUE = (0,255,255)
    YELLOW = (255,255,0)
    WHITE = (255,255,255)
    BLACK = (0,0,0)
    PURPLE = (255,0,255)


class UIPanel(object):
    """Game panel UI"""
    def __init__(self, startPosition):
        super(UIPanel, self).__init__()
        self.startPosition = startPosition
        self.player_ranking_list = [Player("Player1", 2134), Player("Player2", 5841),
                                    Player("Player3", 5896), Player("Player4", 4471),
                                    Player("Player5", 7154), Player("Player6", 8563)]
        self.__exit_button = UIButton("EXIT", startPosition + UI_CONFIG.TITLE_MARGIN, 420, 100, 40)
        self.__ranking = Ranking()
        self.__font_title = pygame.font.SysFont("calibri", 24, bold=True)
        self.__label_font = pygame.font.SysFont("calibri", 18, bold=True)
        self.__score_font = pygame.font.SysFont("calibri", 16, bold=True)


    def show_player_info(self, screen, player):
        player_name = self.__get_label_text(player.name)
        player_score = self.__get_score_text(player.score)

        screen.blit(player_name, (self.startPosition + UI_CONFIG.LABEL_MARGIN, 60))
        screen.blit(player_score, (self.startPosition + UI_CONFIG.SCORE_MARGIN, 80))

    def show_ranking(self, screen):
        name_y_position = 160
        score_y_position = 180
        for player in self.__ranking.player_ranking_list:
            player_name = self.__get_label_text(player.name)
            player_score = self.__get_score_text(player.score)

            screen.blit(player_name, (self.startPosition + UI_CONFIG.LABEL_MARGIN, name_y_position))
            screen.blit(player_score, (self.startPosition + UI_CONFIG.SCORE_MARGIN, score_y_position))
            name_y_position += 40
            score_y_position += 40

    def show_titles(self, screen):
        screen.blit(self.__get_title_text("Player"), (self.startPosition + UI_CONFIG.TITLE_MARGIN, 20))
        screen.blit(self.__get_title_text("Ranking"), (self.startPosition + UI_CONFIG.TITLE_MARGIN, 120))

    def show_buttons(self, screen):
        self.__exit_button.draw_button(screen)

    def check_button_clicked(self):
        if self.__exit_button.check_click():
            game.Game.instance().stop_game()

    def fetch_server_data(self, player):
        self.__ranking.fetch_game_info(player)        

    def check_responses(self):
        self.__ranking.fetch_responses()

    def __get_title_text(self, text):
        return self.__font_title.render(text, True, COLORS.BLUE)

    def __get_label_text(self, text):
        return self.__label_font.render(text, True, COLORS.YELLOW)

    def __get_score_text(self, text):
        return self.__score_font.render(str(text), True, COLORS.WHITE)


class UIButton(object):
    """Game button"""
    def __init__(self, text, x, y, width, height):
        super(UIButton, self).__init__()
        self.__button_font = pygame.font.SysFont("calibri", 18, bold=True)
        self.__button_text = self.__button_font.render(text, True, COLORS.WHITE)
        self.x = x
        self.y = y
        self.width = width
        self.height = height


    def draw_button(self, screen):
        pygame.draw.rect(screen, COLORS.WHITE, self.__get_button_area(), 10)
        pygame.draw.rect(screen, COLORS.BLACK, self.__get_button_area())
        screen.blit(self.__button_text, (self.x + 30, self.y + 10))

    def check_click(self):
        mouse = pygame.mouse.get_pos()
        x,y = mouse[0], mouse[1]

        return x > self.x and x < self.x + self.width and y > self.y and y < self.y + self.height

    def __get_button_area(self):
        return Rect(self.x , self.y, self.width, self.height)



