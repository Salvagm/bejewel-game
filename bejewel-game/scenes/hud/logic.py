from ...services.ranking import RankingService
import json

class Player(object):
    """Current player of the game"""
    def __init__(self, name, score=0):
        super(Player, self).__init__()
        self.name = name
        self.score = score
        self.id = 0


class Ranking(object):
    """Conects with the API and gets other users data"""
    def __init__(self):
        super(Ranking, self).__init__()
        self.player_ranking_list = [Player("Player1", 1234),Player("Player2", 125),Player("Player3", 50)]

    def refresh_top_players(self, session=None, response=None):
        RankingService.get_ranking(self.__update_ranking_list)

    def update_player(self, player):
        RankingService.put_player(player)

    def fetch_game_info(self, player):
        RankingService.put_player(player, self.refresh_top_players)

    def __update_ranking_list(self, session, response):
        json_reponse_list = json.loads(response.text)
        self.player_ranking_list = []
        for player_json in json_reponse_list:
            self.player_ranking_list.append(Player(player_json["name"], player_json["score"]))

