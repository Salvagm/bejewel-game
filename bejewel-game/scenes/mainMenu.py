import pygame
import os

from pygame.locals import *
from .scene import Scene
from .level import Level
from .hud import Player, COLORS
from ..lib import TextInput
from .. import game
from ..services.ranking import RankingService


class MainMenu(Scene):
    """docstring for MianMeny"""
    def __init__(self):
        super(MainMenu, self).__init__()
        self.textinput = TextInput(font_family="calibri", font_size=24, text_color=COLORS.BLUE)
        self.__menu_font = pygame.font.SysFont("calibri", 24)
        self.__titleSprite = pygame.image.load(os.path.join('bejewel-game','resources','title.png'))
        # self.__titleSprite = pygame.transform.scale2x(self.__titleSprite)
        self.__start_game = False


    def render(self, screen):
        screen.fill(COLORS.BLACK)

        # render text
        label = self.__menu_font.render("Type your name", 1, COLORS.WHITE)
        label_note = self.__menu_font.render("(Enter to start)", 1, COLORS.WHITE)
        screen.blit(self.__titleSprite, (90,20))
        screen.blit(label, (270, 350))
        screen.blit(label_note, (270,380))
        screen.blit(self.textinput.get_surface(), (270, 420))

        pygame.display.flip()

    def update(self):
        if self.__start_game:
            self.__create_player()
            game.Game.instance().change_scene("level")

    def handle_events(self, events):

        if len(self.textinput.get_text()) > 10:
            print(self.textinput.get_text())
            backspace_event = pygame.event.Event(pygame.KEYDOWN, key=K_BACKSPACE)
            backspace_event.unicode = '\x08'

            pygame.event.post(backspace_event)
            # events.append(backspace_event)
        self.__start_game = self.textinput.update(events)

    def __create_player(self):
        player_name = self.textinput.get_text()
        game.Game.instance().player.name = player_name
        
        request = RankingService.post_player(game.Game.instance().player)
        try:
            
            response = request.result()
            player_id = response.text
        except Exception as e:
            player_id = 0
        
        game.Game.instance().player.id = player_id


