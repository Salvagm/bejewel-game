import random
import heapq
import os

import pygame
from pygame.locals import *


SHAPE_TYPES = [
    ('S', pygame.image.load(os.path.join('bejewel-game', 'resources', 'square-small.png'))),  # Square
    ('D', pygame.image.load(os.path.join('bejewel-game', 'resources', 'diamond-small.png'))),  # Diamon
    ('C', pygame.image.load(os.path.join('bejewel-game', 'resources', 'circle-small.png'))),  # Circle
    ('H', pygame.image.load(os.path.join('bejewel-game', 'resources', 'star-small.png'))),  # Star
    ('P', pygame.image.load(os.path.join('bejewel-game', 'resources', 'pentagon-small.png'))),  # Pentagon
    (' ', None) # EMPTY
]

class DIRECTION:
    UP = (0,-1)
    DOWN = (0,1)
    RIGHT = (1,0)
    LEFT = (-1,0)
    HORIZONTAL = ((1,0), (-1,0))  # Right, left
    VERTICAL = ((0,-1), (0,1))  # Up, Down

class States:
    READY = 1
    REVERT_MATCHING = 2
    FILLING = 3
    STATIC_MATCHING = 4

class Shape(object):
    """docstring for Shape"""
    def __init__(self, shape_type):
        super(Shape, self).__init__()
        self.shape_type = shape_type

    def __str__(self):
        return self.shape_type[0]

    def __eq__(self, other_shape):
        return self.shape_type[0] == other_shape.shape_type[0]

    def get_sprite(self):
        return self.shape_type[1]

class Cell(object):
    """docstring for Cell"""

    CELL_SCORE = 10

    def __init__(self, position, world_position, shape):
        super(Cell, self).__init__()
        self.position = position
        self.world_position = world_position
        self.__shape = shape
        self.is_empty = False

    def __repr__(self):
        cell_info = "Position: (%d, %d), Empty: %r, Shape: %s" % (*self.position, self.is_empty, self.__shape)
        return cell_info

    def __str__(self):
        cell_info = '[' + str(self.__shape) + ']'
        return cell_info

    def __lt__(self, other):
        return self.position[1] < other.position[1]

    def __eq__(self, other):
        return self.position == other.position

    def set_shape_type(self, new_shape_type):
        self.__shape.shape_type = new_shape_type
        self.is_empty = False

    def clean_cell(self):
        self.__shape.shape_type = SHAPE_TYPES[5]
        self.is_empty = True

    def get_shape_sprite(self):
        return self.__shape.get_sprite()

    def swap_shapes(self, other_cell, is_changed=True):
        self.__shape, other_cell.__shape = other_cell.__shape, self.__shape
        self.is_empty, other_cell.is_empty = other_cell.is_empty, self.is_empty

        self.is_changed = is_changed
        other_cell.is_changed = is_changed

    def is_equal_shape(self, other_cell):
        return self.__shape == other_cell.__shape

    def is_near(self, other):
        d1 = (self.position[0] - other.position[0]) * (self.position[0] - other.position[0])
        d2 = (self.position[1] - other.position[1]) * (self.position[1] - other.position[1])

        distance = d1 + d2

        return distance == 1


class Board(object):
    """Board game"""
    def __init__(self, size, tile_size):
        super(Board, self).__init__()
        self.board_state = States.READY
        self.__board_matrix = []
        self.__moved_cell_list = []
        self.__fail_matched_list = []
        self.__empty_cell_list = []
        self.__board_size = size
        self.tiles_size = tile_size
        self.width, self.height = size * tile_size, size * tile_size

        self.generate_board()

    def __str__(self):
        board_state = ""

        for row in self.__board_matrix:
            board_state += '|'
            for cell in row:
                board_state += str(cell)
            board_state += '|\n'

        return board_state


    def swap_cell(self, orig_cell, dest_cell):
        has_match = True

        if self.board_state == States.READY and orig_cell.is_near(dest_cell):
            self.board_state = States.REVERT_MATCHING
            orig_cell.swap_shapes(dest_cell)
            self.__moved_cell_list.append(dest_cell)
            self.__moved_cell_list.append(orig_cell)

    def restore_movement(self):
        self.board_state = States.READY
        self.__fail_matched_list[0].swap_shapes(self.__fail_matched_list[1])
        self.__fail_matched_list = []


    def check_match(self):
        score = 0
        if self.board_state == States.REVERT_MATCHING and len(self.__moved_cell_list) > 0:
            cell = self.__moved_cell_list.pop(0)
            matched_cell_list = []

            matched_cell_list.extend(self.__get_match_list(cell, DIRECTION.HORIZONTAL))
            matched_cell_list.extend(self.__get_match_list(cell, DIRECTION.VERTICAL))

            self.__try_store_match_cell_list(matched_cell_list, cell)

            if len(self.__fail_matched_list) > 1:
                self.restore_movement()

        elif self.board_state == States.REVERT_MATCHING and len(self.__moved_cell_list) == 0:
            self.__fail_matched_list = []
            heapq.heapify(self.__empty_cell_list)
            self.board_state = States.FILLING
            score = self.__calculate_score()


        return score
    def generate_board(self):
        for y in range(self.__board_size):
            row = []

            for x in range(self.__board_size):
                new_shape = self.__get_random_shape()
                cell = Cell((x,y),(x*self.tiles_size, y*self.tiles_size), Shape(new_shape))
                row.append(cell)
                # print(repr(cell))

            self.__board_matrix.append(row)

    def get_cell_by_position(self, position):
        if(self.__in_range(position[0]) and self.__in_range(position[1])):
            return self.__board_matrix[position[1]][position[0]]

    def show_board(self, screen):
        for y in range(self.__board_size):
            for x in range(self.__board_size):
                cell = self.__board_matrix [y][x]

                sprite = cell.get_shape_sprite()
                if sprite:
                    screen.blit(sprite, cell.world_position)

    def fill_board(self):
        direction = DIRECTION.UP
        next_cell = None
        if self.board_state == States.FILLING:
            if not len(self.__empty_cell_list) == 0:
                cell = heapq.heappop(self.__empty_cell_list)

                current_position = list(cell.position)
                current_position = [current_position[0] + direction[0], current_position[1] + direction[1]]

                next_cell = self.get_cell_by_position(current_position)
                if not next_cell:
                    cell.set_shape_type(self.__get_random_shape())
                elif not next_cell.is_empty:
                    cell.swap_shapes(next_cell)
                    heapq.heappush(self.__empty_cell_list, next_cell)

                # print(self)

            else:
                self.board_state = States.READY

    def __calculate_score(self):
        num_cell_matched = len(self.__empty_cell_list)
        multiplier = 1

        if num_cell_matched > 3:
            multiplier += 0.1 * num_cell_matched

        return int(Cell.CELL_SCORE * num_cell_matched * multiplier)

    def __get_match_list(self, cell, direction):
        matched_cell_list = [cell]

        self.__get_cells_by_direction(cell, direction[0], matched_cell_list)
        self.__get_cells_by_direction(cell, direction[1], matched_cell_list)

        if len(matched_cell_list) > 2:
            return matched_cell_list

        return []

    def __get_cells_by_direction(self, cell, direction, cell_list):

        is_match = True

        current_position = list(cell.position)

        while is_match:
            current_position = [current_position[0] + direction[0], current_position[1] + direction[1]]
            next_cell = self.get_cell_by_position(current_position)

            if next_cell and cell.is_equal_shape(next_cell) and next_cell not in cell_list:
                cell_list.append(next_cell)
            else:
                is_match = False


        return cell_list

    def __try_store_match_cell_list(self, matched_cell_list, orgi_cell):
        if len(matched_cell_list) > 2:
            for matched_cell in matched_cell_list:
                matched_cell.clean_cell()
                if matched_cell not in self.__empty_cell_list:
                    self.__empty_cell_list.append(matched_cell)
        else:
            self.__fail_matched_list.append(orgi_cell)


    @staticmethod
    def __get_random_shape():
        shape = random.randint(0, 4)

        return SHAPE_TYPES[shape]

    def __in_range(self, position):
        return position >= 0 and position < self.__board_size