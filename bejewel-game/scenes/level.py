
import pygame
from pygame.locals import *

from .scene import Scene
from .board import Board
from .hud import UIPanel, COLORS
from ..services.ranking import RankingService
from .. import CONFIG, game

class Level(Scene):
    """Main game leve"""
    def __init__(self):
        super(Level, self).__init__()
        self.player = game.Game.instance().player
        self.board = Board(CONFIG.BOARD_SIZE, CONFIG.TILE_WIDTH)
        self.ui_panel = UIPanel(CONFIG.TILE_WIDTH * CONFIG.BOARD_SIZE + 10)
        self.selection_list = []

        self.ui_panel.fetch_server_data(self.player)
        

    def render(self, screen):
        screen.fill(COLORS.BLACK)

        self.__show_selection(screen)

        self.board.show_board(screen)
        self.ui_panel.show_titles(screen)
        self.ui_panel.show_player_info(screen, self.player)
        self.ui_panel.show_ranking(screen)
        self.ui_panel.show_buttons(screen)

        pygame.display.flip()

    def update(self):
        if len(self.selection_list) >= 2:
            cell_orig = self.board.get_cell_by_position(self.selection_list[0])
            cell_dest = self.board.get_cell_by_position(self.selection_list[1])

            if(cell_orig and cell_dest):
                self.board.swap_cell(cell_orig, cell_dest)

            self.selection_list = []

        match_score = self.board.check_match()
        if match_score > 0:
            self.player.score += match_score
            self.ui_panel.fetch_server_data(self.player)

        self.board.fill_board()

    def handle_events(self, events):

        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # print(pygame.mouse.get_pos())
                self.ui_panel.check_button_clicked()
                self.__do_click()

            if event.type == pygame.MOUSEBUTTONUP and len(self.selection_list) > 0:
                # print(pygame.mouse.get_pos())
                self.__do_drag()

    def __do_drag(self):
        x = pygame.mouse.get_pos()[0] // CONFIG.TILE_WIDTH
        y = pygame.mouse.get_pos()[1] // CONFIG.TILE_HEIGHT
        direction = self.__get_direction_vector(self.selection_list[0], (x,y))
        next_position = self.__get_next_position_by_direction(self.selection_list[0], direction)

        if next_position[0] <= 8 and next_position[1] <= 8 and next_position not in self.selection_list:
            self.selection_list.append(next_position)

    def __do_click(self):
        mouse = pygame.mouse.get_pos()
        x = mouse[0] // CONFIG.TILE_WIDTH
        y = mouse[1] // CONFIG.TILE_HEIGHT

        if self.board.get_cell_by_position((x,y)) and mouse[0] < self.board.width:
            self.selection_list.append((x,y))

    def __show_selection(self, screen):
        for selection in self.selection_list:
            selection_rect = Rect(selection[0]*CONFIG.TILE_WIDTH, selection[1]*CONFIG.TILE_HEIGHT,
                                  CONFIG.TILE_WIDTH, CONFIG.TILE_HEIGHT)
            pygame.draw.rect(screen, COLORS.WHITE, selection_rect, CONFIG.SELECTION_LINE_SIZE)

    @staticmethod
    def __get_direction_vector(init_position, dest_position):
        x, y = dest_position[0] - init_position[0] , dest_position[1] - init_position[1]

        x = x//abs(x) if x else 0
        y = y//abs(y) if y else 0

        return (x,y)

    @staticmethod
    def __get_next_position_by_direction(init_position, direction):
        x, y = init_position

        if direction[0] is not 0:
            x = init_position[0] + direction[0]
        elif direction[1] is not 0:
            y = init_position[1] + direction[1]

        return (x,y)